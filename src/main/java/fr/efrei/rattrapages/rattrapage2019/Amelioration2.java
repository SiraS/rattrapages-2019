package fr.efrei.rattrapages.rattrapage2019;

public class Amelioration2 {
    public void repartir(int[] montants, int[] limiteMontants) {
        int exces = 0;
        for (int i = 0; i < montants.length; i++) {
            if (montants[i] > limiteMontants[i])
                exces += montants[i] - limiteMontants[i];
        }

        int manque = 0;
        for (int i = 0; i < montants.length; i++) {
            if (limiteMontants[i] > montants[i])
                manque += limiteMontants[i] - montants[i];
        }

        int excesRestantADistribuer = Math.min(exces, manque);
        int excesRestantAPrelever = excesRestantADistribuer;

        for (int i = 0; i < montants.length; i++) {
            if (montants[i] > limiteMontants[i]) {
                int excesAPrelever = Math.min(montants[i] - limiteMontants[i], excesRestantAPrelever);

                montants[i] -= excesAPrelever;
                excesRestantAPrelever -= excesAPrelever;
            } else if (montants[i] < limiteMontants[i]) {
                int excesADistribuer = Math.min(limiteMontants[i] - montants[i], excesRestantADistribuer);

                montants[i] += excesADistribuer;
                excesRestantADistribuer -= excesADistribuer;
            }
        }
    }
}
