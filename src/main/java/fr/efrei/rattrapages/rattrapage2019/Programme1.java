package fr.efrei.rattrapages.rattrapage2019;

import java.util.ArrayList;
import java.util.List;

public class Programme1 {
    public static void main(String[] args) {
        List<Road> routes = new ArrayList<>();
        routes.add(new Road(30));
        routes.add(new Road(50));
        routes.add(new Highway(60));
        routes.add(new Street(1));

        Vehicle car = new Vehicle(130); // voiture
        Vehicle smallMotorbike = new Vehicle(45); // moto < 125cc

        System.out.println("Voiture sur route #0 par temps clair : " + car.getEstimatedHours(routes.get(0), Weather.NORMAL));
        System.out.println("Voiture sur route #0 par temps de pluie : " + car.getEstimatedHours(routes.get(0), Weather.RAINY));
        System.out.println("Voiture sur route #1 par temps clair : " + car.getEstimatedHours(routes.get(1), Weather.NORMAL));
        System.out.println("Voiture sur route #1 par temps de pluie : " + car.getEstimatedHours(routes.get(1), Weather.RAINY));
        System.out.println("Voiture sur autoroute par temps clair : " + car.getEstimatedHours(routes.get(2), Weather.NORMAL));
        System.out.println("Voiture sur autoroute par temps de pluie : " + car.getEstimatedHours(routes.get(2), Weather.RAINY));
        System.out.println("Voiture sur route de ville par temps clair : " + car.getEstimatedHours(routes.get(3), Weather.NORMAL));
        System.out.println("Voiture sur route de ville par temps de pluie : " + car.getEstimatedHours(routes.get(3), Weather.RAINY));

        System.out.println("Petite moto sur route #0 par temps clair : " + smallMotorbike.getEstimatedHours(routes.get(0), Weather.NORMAL));
        System.out.println("Petite moto sur route #0 par temps de pluie : " + smallMotorbike.getEstimatedHours(routes.get(0), Weather.RAINY));
        System.out.println("Petite moto sur route #1 par temps clair : " + smallMotorbike.getEstimatedHours(routes.get(1), Weather.NORMAL));
        System.out.println("Petite moto sur route #1 par temps de pluie : " + smallMotorbike.getEstimatedHours(routes.get(1), Weather.RAINY));
        System.out.println("Petite moto sur autoroute par temps clair : " + smallMotorbike.getEstimatedHours(routes.get(2), Weather.NORMAL));
        System.out.println("Petite moto sur autoroute par temps de pluie : " + smallMotorbike.getEstimatedHours(routes.get(2), Weather.RAINY));
        System.out.println("Petite moto sur route de ville par temps clair : " + smallMotorbike.getEstimatedHours(routes.get(3), Weather.NORMAL));
        System.out.println("Petite moto sur route de ville par temps de pluie : " + smallMotorbike.getEstimatedHours(routes.get(3), Weather.RAINY));

        /* Résultat attendu :
                Voiture sur route #0 par temps clair : 20.0
                Voiture sur route #0 par temps de pluie : 22.5
                Voiture sur route #1 par temps clair : 33.333333333333336
                Voiture sur route #1 par temps de pluie : 37.5
                Voiture sur autoroute par temps clair : 27.692307692307693
                Voiture sur autoroute par temps de pluie : 32.72727272727273
                Voiture sur route de ville par temps clair : 1.2
                Voiture sur route de ville par temps de pluie : 1.2
                Petite moto sur route #0 par temps clair : 40.0
                Petite moto sur route #0 par temps de pluie : 40.0
                Petite moto sur route #1 par temps clair : 66.66666666666667
                Petite moto sur route #1 par temps de pluie : 66.66666666666667
                Petite moto sur autoroute par temps clair : 80.0
                Petite moto sur autoroute par temps de pluie : 80.0
                Petite moto sur route de ville par temps clair : 1.3333333333333333
                Petite moto sur route de ville par temps de pluie : 1.3333333333333333
         */
    }

    public static class Vehicle {
        private final int maximumSpeed;

        public Vehicle(int maximumSpeed) {
            this.maximumSpeed = maximumSpeed;
        }

        public double getEstimatedHours(Road route, Weather weather) {
            int speed = Math.min(maximumSpeed, route.getMaximumSpeed(weather));
            return 60.0 * route.getDistance() / speed;
        }
    }

    // Route classique (avec terre-plein central)
    public static class Road {
        // TODO : ENRICHIR CETTE CLASSE
    }

    // Autoroute
    public static class Highway extends Road {
        // TODO : ENRICHIR CETTE CLASSE
    }

    // Road de ville
    public static class Street extends Road {
        // TODO : ENRICHIR CETTE CLASSE
    }

    public static enum Weather {
        NORMAL,
        RAINY; // Pluvieux
    }
}
