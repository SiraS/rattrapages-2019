package fr.efrei.rattrapages.rattrapage2019;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Programme2 {
    public static void main(String[] args) {
        List<Student> students = new ArrayList<>();
        students.add(new Student("John Doe", 14, 16));
        students.add(new Student("Jane Doe", 20, 2));
        students.add(new Student("John Smith", 7, 18));
        students.add(new Student("Jack Doe", 12, 10));
        students.add(new Student("Jack Smith", 16, 16));
        students.add(new Student("Jane Smith", 9, 14));

        System.out.println("Moyenne générale : " + getAverageGrade(students));
        System.out.println("Top 3 : ");

        for(String detailedLine: getTop3StudentDetails(students)) {
            System.out.println(detailedLine);
        }

        /* Résultat attendu :
                Moyenne générale : 12.833333333333334
                Top 3 :
                Jack Smith with 16.0
                John Doe with 15.0
                John Smith with 12.5
         */
    }

    private static double getAverageGrade(List<Student> students) {
        // TODO : IMPLEMENTER CETTE METHODE
    }

    private static List<String> getTop3StudentDetails(List<Student> students) {
        // TODO : IMPLEMENTER CETTE METHODE
    }

    public static class Student {
        private final String name;
        private final List<Integer> grades;

        public Student(String name, Integer... grades) {
            this.name = name;
            this.grades = Arrays.asList(grades);
        }

        public String getName() {
            return name;
        }

        public double getAverageGrade() {
            int total = 0;
            for (int grade: grades) {
                total += grade;
            }
            return 1.0 * total / grades.size();
        }
    }
}
